import { initializeApp } from 'firebase/app';

import { getAuth, signInWithRedirect, signInWithPopup, 
         GoogleAuthProvider, createUserWithEmailAndPassword, 
         signInWithEmailAndPassword, signOut, onAuthStateChanged } from 'firebase/auth';

import { getFirestore, doc, getDoc, setDoc, 
         collection, writeBatch, query, getDocs } from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDO_NL-HbkHF5pIfndhlXhKyPeINySRjMs",
    authDomain: "crown-clothing-db-32c8f.firebaseapp.com",
    projectId: "crown-clothing-db-32c8f",
    storageBucket: "crown-clothing-db-32c8f.appspot.com",
    messagingSenderId: "1010172802826",
    appId: "1:1010172802826:web:0613ce6c8446fc54e52cf3"
};
  
const firebaseApp = initializeApp(firebaseConfig);
console.log("firebaseApp:", firebaseApp);

const googleProvider = new GoogleAuthProvider();

googleProvider.setCustomParameters({
    prompt: "select_account"
});

export const auth = getAuth();
export const signInWithGooglePopup = () => signInWithPopup(auth, googleProvider);
export const signInWithGoogleRedirect = () => signInWithRedirect(auth, googleProvider);

export const db = getFirestore();

export const addCollectionAndDocuments = async (collectioKey, objectsToAdd) => {
    const collectionRef = collection(db, collectioKey);
    const batch = writeBatch(db);

    objectsToAdd.forEach(object => {
        const docRef = doc(collectionRef, object.title.toLowerCase());
        batch.set(docRef, object);
    });

    await batch.commit();
    console.log("done");
};

export const getCategoriesAndDocuments = async () => {

    const collectionRef = collection(db, 'categories');
    const q = query(collectionRef);
    const querySnapshot = await getDocs(q);

    return querySnapshot.docs.map(docSnapshot => docSnapshot.data());

};

export const createUserDocumentFromAuth = async (userAuth, additionalInformation = {}) => {

    const userDocRef = doc(db, 'users', userAuth.uid);
    console.log(userDocRef);

    const userSnapshop = await getDoc(userDocRef);
    console.log(userSnapshop);
    console.log(userSnapshop.exists());

    if (!userSnapshop.exists()) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();

        try {
            await setDoc(userDocRef, { displayName, email, createdAt, ...additionalInformation })
        } catch (error) {
            console.log("error creating user", error);
        }
    }
};

export const createAuthUserWithEmailAndPassword = async (email, password) => {
    if (!email || !password) return; // if we don't get an email or password, we also want a return
    return createUserWithEmailAndPassword(auth, email, password);
};

export const signInAuthUserWithEmailAndPassword = async (email, password) => {
    if (!email || !password) return;
    return signInWithEmailAndPassword(auth, email, password);
};

export const signOutUser = async () => await signOut(auth);

export const onAuthStateChangeListener = (callback) => onAuthStateChanged(auth, callback);

export const getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const unsubscribe = onAuthStateChanged(
            auth,
            (userAuth) => {
                unsubscribe();
                resolve(userAuth);
            },
            reject
        );
    });
};