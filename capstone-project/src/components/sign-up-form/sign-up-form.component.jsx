import React, { useState } from 'react';
import './sign-up-form.styles.scss';
import FormInput from '../../components/form-input/form-input.component';
import Button from '../button/button.component';
import { useDispatch } from 'react-redux';
import { signUpStart } from '../../store/user/user.action';

const defaultFormFields = {
    displayName: "",
    email: "",
    password: "",
    confirmPassword: "",
};

const SignUpForm = () => {

    const [formFields, setFormFields] = useState(defaultFormFields);
    const dispatch = useDispatch();
    const { displayName, email, password, confirmPassword } = formFields; // Destructuring

    console.log("hit");

    const resetFormFields = () => {
        setFormFields(defaultFormFields);
    };

    const handleSubmit = async (event) => { //it's an async method because we're generating a document inside of an external service
        event.preventDefault();

        if (password !== confirmPassword) {
            alert("Password does not match!");
            return;
        }

        try {
            dispatch(signUpStart(email, password, displayName));
            resetFormFields();

        } catch (error) {
            if (error.code === "auth/email-already-in-use") {
                alert("Cannot create user, email already in use");
            } else {
                console.error("User creation encountered an error", error);
            }
        }
    };

    const handleChange = (event) => {
        const {name, value} = event.target; // Destructuring
        setFormFields({...formFields, [name]: value});
    };

    return (
        <div>
            <h2>Don't have an account?</h2>
            <span>Sign Up with your email and password</span>
            <form onSubmit={handleSubmit}> 
                <FormInput label="Display Name" type="text" required onChange={handleChange} 
                           name="displayName" value={displayName}/>

                <FormInput label="Email" type="email" required onChange={handleChange} 
                           name="email" value={email}/>

                <FormInput label="Password" type="password" required onChange={handleChange} 
                           name="password" value={password}/>

                <FormInput label="Confirm Password" type="password" required onChange={handleChange} 
                           name="confirmPassword" value={confirmPassword}/>

                <Button buttonType="google" type="submit">Sign Up</Button>
            </form>
        </div>
    )
};
export default SignUpForm;