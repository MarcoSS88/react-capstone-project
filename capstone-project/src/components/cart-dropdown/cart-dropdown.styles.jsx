import styled from 'styled-components';

import {
  BaseButton,
  GoogleSignInButton,
  InvertedButton
} from '../button/button.styles';

export const CartDropdownContainer = styled.div`
  position: absolute;
  width: 240px;
  height: 340px;
  display: flex;
  flex-direction: column;
  padding: 20px;
  border: 1px solid black;
  background-color: white;
  top: 90px;
  right: 40px;
  z-index: 5;

  ${BaseButton},
  ${GoogleSignInButton},
  ${InvertedButton} {
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace; 
    font-size: 14.3px;  
    font-weight: lighter; 
    margin-top: auto;

    &:hover {
      display: flex;
      align-items: center;
      justify-content: center;
      font-size: 14.1px;  
      font-weight: lighter; 
    }
  }
`;

export const EmptyMessage = styled.span`
  font-size: 18px;
  margin: 50px auto;
`;

export const CartItems = styled.div`
  height: 240px;
  display: flex;
  flex-direction: column;
  overflow: scroll;
`;