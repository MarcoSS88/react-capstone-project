/* import { compose, applyMiddleware } from 'redux';
import { configureStore } from '@reduxjs/toolkit'
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import logger from 'redux-logger';
import { rootReducer } from './root-reducer';

//import thunk from 'redux-thunk'; sostituiremo saga a thunk!

import createSagaMiddleware from 'redux-saga'; // per farlo funzionare abbiamo bisogno di {rootSaga}
import { rootSaga } from './root-saga';

const persistConfig = {
    key: "root", 
    storage: storage, 
    whitelist: ["cart"]
};

const sagaMiddleware = createSagaMiddleware();

const persistedReducer = persistReducer(persistConfig, rootReducer); */

// Se falso non restituisce niente, se vero restituisce ciò che c'è all'interno di middleWares
//const middleWares = [process.env.NODE_ENV !== "production" && logger, thunk].filter(Boolean);
// il middleWare è una sorta di aiutante della libreria che lavora prima che un'action venga invocata nel reducer
/* const thunkMiddleware = (store) => (next) => (action) => {
    if (typeof(action) === "function") {
        action(dispatch);
    }
}; */

/* const middleWares = [process.env.NODE_ENV !== "production" && logger, sagaMiddleware].filter(Boolean);

const composeEnhancer = (process.env.NODE_ENV !== "production" && window && window.__REDUX_DEVTOOLS_EXTENTION_COMPOSE__) || compose;

const composeEnhancers = composeEnhancer(applyMiddleware(...middleWares));

export const store = configureStore({ reducer: persistedReducer }, undefined, composeEnhancers);

sagaMiddleware.run(rootSaga);

export const persistor = persistStore(store);  */

//////////////////////////////////////////////////////////////

import { configureStore } from '@reduxjs/toolkit';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { rootReducer } from './root-reducer';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from './root-saga';
 
const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['cart'],
};
 
const sagaMiddleware = createSagaMiddleware();
const persistedReducer = persistReducer(persistConfig, rootReducer);
 
const middleWares = [process.env.NODE_ENV !== 'production' && logger,
 
  (process.env.NODE_ENV !== 'production' &&
    window &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
    sagaMiddleware) ||
    false,
].filter(Boolean);
 
 
export const store = configureStore({
  reducer: persistedReducer,
  middleware: middleWares,
});
 
sagaMiddleware.run(rootSaga);
 
export const persistor = persistStore(store);